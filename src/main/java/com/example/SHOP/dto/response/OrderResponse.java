package com.example.SHOP.dto.response;

import java.sql.Timestamp;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class OrderResponse {

    private Long orderId;
    private String customerName;
    private String itemName;
    private Integer quantity;
    private Double totalPrice;
    private Timestamp orderDate;
    
}
