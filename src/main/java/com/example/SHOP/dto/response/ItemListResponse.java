package com.example.SHOP.dto.response;

import java.util.List;

import com.example.SHOP.dto.pagination.ItemPagination;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ItemListResponse {

    private List<ItemResponse> data;
    private ItemPagination pagination;
    
}
