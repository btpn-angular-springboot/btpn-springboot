package com.example.SHOP.dto.response;

import java.util.List;

import com.example.SHOP.dto.pagination.CustomerPagination;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CustomerListResponse {
    
    private List<CustomerResponse> data;
    private CustomerPagination pagination;
}
