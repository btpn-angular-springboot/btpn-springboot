package com.example.SHOP.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AddOrderRequest {

    private Long customerId;
    private Long itemId;
    private Integer quantity;
    private Double totalPrice;

    public void validate() {

        if (customerId == null) {
            throw new IllegalArgumentException("Customer id cannot be null");
        }
        if (itemId == null) {
            throw new IllegalArgumentException("Item id cannot be null");
        }
        if (quantity == null) {
            throw new IllegalArgumentException("Quantity cannot be null");
        }
        if (totalPrice == null) {
            throw new IllegalArgumentException("Total price cannot be null");
        }
        
    }
}
