package com.example.SHOP.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AddItemRequest {

    private String itemName;
    private Integer itemStock;
    private Double itemPrice;

    public void validate() {

        if (itemName == null || itemName.isEmpty()) {
            throw new IllegalArgumentException("Item name cannot be null or empty");
        }
        if (itemStock == null) {
            throw new IllegalArgumentException("Item stock cannot be null");
        }
        if (itemPrice == null) {
            throw new IllegalArgumentException("Item price cannot be null");
        }
        
    }
}
