package com.example.SHOP.services;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.example.SHOP.dto.request.AddCustomerRequest;
import com.example.SHOP.dto.request.CustomerRequest;
import com.example.SHOP.dto.request.UpdateCustomerRequest;
import com.example.SHOP.dto.response.CustomerResponse;
import com.example.SHOP.models.CustomerModel;
import com.example.SHOP.repositories.CustomerRepository;

@Service
public class CustomerServiceImplement implements ICustomerService {

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private MinioService minioService;

    @Override
    public Page<CustomerModel> findAllCustomers(Pageable pageable) {
        return customerRepository.findAllByIsActiveTrue(pageable);
    }

    @Override
    public Page<CustomerModel> findCustomersByName(String customerName, Pageable pageable) {
        return customerRepository.findByCustomerNameContainingIgnoreCase(customerName, pageable);
    }

    @Override
    public List<CustomerResponse> convertToCustomerResponse(Page<CustomerModel> customerModels) {
        return customerModels.stream()
                .filter(CustomerModel::getIsActive)
                .map(this::mapToCustomerResponse)
                .collect(Collectors.toList());
    }

    @Override
    public CustomerResponse findCustomerByCustomerId(CustomerRequest customerRequest) {
        Long customerId = customerRequest.getCustomerId();
        CustomerModel existingCustomer = customerRepository.findById(customerId)
                .orElseThrow(() -> new IllegalArgumentException("Customer not found with id: " + customerId));

        if (!existingCustomer.getIsActive()) {
            throw new IllegalArgumentException("Customer with ID " + customerId + " is not active.");
        }

        return mapToCustomerResponse(existingCustomer);
    }

    private CustomerResponse mapToCustomerResponse(CustomerModel customerModel) {
        return new CustomerResponse(
                customerModel.getCustomerId(),
                customerModel.getCustomerName(),
                customerModel.getCustomerAddress(),
                customerModel.getPhoneNumber(),
                customerModel.getIsActive(),
                customerModel.getLastOrder(),
                customerModel.getPicture());

    }

    @Override
    public CustomerModel addCustomer(AddCustomerRequest addCustomerRequest) {
        addCustomerRequest.validate();
        CustomerModel customerModel = new CustomerModel();
        customerModel.setCustomerName(addCustomerRequest.getCustomerName());
        customerModel.setCustomerAddress(addCustomerRequest.getCustomerAddress());
        customerModel.setPhoneNumber(addCustomerRequest.getPhoneNumber());

        try {
            if (addCustomerRequest.getPicture() != null) {
                String picUrl = minioService.uploadFile(addCustomerRequest.getPicture());
                customerModel.setPicture(picUrl);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        customerModel.setIsActive(true);
        return customerRepository.save(customerModel);
    }

    @Override
    public CustomerModel updateCustomer(Long customerId, UpdateCustomerRequest updateCustomerRequest) {
        updateCustomerRequest.validate();
        CustomerModel existingCustomer = customerRepository.findById(customerId)
                .orElseThrow(() -> new IllegalArgumentException("Customer not found with id: " + customerId));

        if (!existingCustomer.getIsActive()) {
            throw new IllegalArgumentException("Inactive customer cannot be updated with id: " + customerId);
        }
        existingCustomer.setCustomerName(updateCustomerRequest.getCustomerName());
        existingCustomer.setCustomerAddress(updateCustomerRequest.getCustomerAddress());
        existingCustomer.setPhoneNumber(updateCustomerRequest.getPhoneNumber());

        try {
            if (updateCustomerRequest.getPicture() != null) {
                String picUrl = minioService.uploadFile(updateCustomerRequest.getPicture());
                existingCustomer.setPicture(picUrl);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        CustomerModel updatedCustomer = customerRepository.save(existingCustomer);

        return updatedCustomer;
    }

    @Override
    public CustomerModel deleteCustomer(Long customerId) {
        CustomerModel existingCustomer = customerRepository.findById(customerId)
                .orElseThrow(() -> new IllegalArgumentException("Customer not found with id: " + customerId));
        existingCustomer.setIsActive(false);
        return customerRepository.save(existingCustomer);
    }

}
