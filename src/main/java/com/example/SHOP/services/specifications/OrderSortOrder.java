package com.example.SHOP.services.specifications;

import org.springframework.data.domain.Sort;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum OrderSortOrder {
    
    CUSTOMER_NAME_ASC("customer.customerName", Sort.Direction.ASC),
    CUSTOMER_NAME_DESC("customer.customerName", Sort.Direction.DESC),
    ITEM_NAME_ASC("item.itemName", Sort.Direction.ASC),
    ITEM_NAME_DESC("item.itemName", Sort.Direction.DESC),
    FORMAT_TO_DATE_TIME_ASC("orderDate", Sort.Direction.ASC),
    FORMAT_TO_DATE_TIME_DESC("orderDate", Sort.Direction.DESC),
    QUANTITY_ASC("quantity", Sort.Direction.ASC),
    QUANTITY_DESC("quantity", Sort.Direction.DESC),
    FORMATTED_TOTAL_PRICE_ASC("totalPrice", Sort.Direction.ASC),
    FORMATTED_TOTAL_PRICE_DESC("totalPrice", Sort.Direction.DESC),;
    
    private final String fieldName;
    private final Sort.Direction direction;
}
