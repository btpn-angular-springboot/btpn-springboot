package com.example.SHOP.services.specifications;

import org.springframework.data.domain.Sort;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum ItemSortOrder {
    
    ITEM_NAME_ASC("itemName", Sort.Direction.ASC),
    ITEM_NAME_DESC("itemName", Sort.Direction.DESC),
    FORMATTED_PRODUCT_PRICE_ASC("itemPrice", Sort.Direction.ASC),
    FORMATTED_PRODUCT_PRICE_DESC("itemPrice", Sort.Direction.DESC),
    ITEM_STOCK_ASC("itemStock", Sort.Direction.ASC),
    ITEM_STOCK_DESC("itemStock", Sort.Direction.DESC),
    FORMATTED_PRODUCT_LAST_RESTOCK_ASC("itemLastRestock", Sort.Direction.ASC),
    FORMATTED_PRODUCT_LAST_RESTOCK_DESC("itemLastRestock", Sort.Direction.DESC),
    IS_AVAILABLE_DESC("isAvailable", Sort.Direction.DESC),
    IS_AVAILABLE_ASC("isAvailable", Sort.Direction.ASC),;

    private final String fieldName;
    private final Sort.Direction direction;
}
