package com.example.SHOP.services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.example.SHOP.dto.request.AddOrderRequest;
import com.example.SHOP.dto.request.OrderRequest;
import com.example.SHOP.dto.request.UpdateOrderRequest;
import com.example.SHOP.dto.response.OrderResponse;
import com.example.SHOP.models.OrderModel;

@Service
public interface IOrderService {

    Page<OrderModel> findAllOrders(Pageable pageable);

    Page<OrderModel> findOrdersByCustomerName(String customerName, Pageable pageable);

    List<OrderResponse> convertToOrderResponse(Page<OrderModel> orderModels);

    OrderResponse findByOrderByOrderId(OrderRequest orderRequest);

    OrderModel addOrder(AddOrderRequest addOrderRequest);

    OrderModel updateOrder(Long orderId, UpdateOrderRequest updateOrderRequest);

    OrderModel deleteOrder(Long orderId);
    
}
