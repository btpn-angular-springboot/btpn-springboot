package com.example.SHOP.services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.example.SHOP.dto.request.AddCustomerRequest;
import com.example.SHOP.dto.request.CustomerRequest;
import com.example.SHOP.dto.request.UpdateCustomerRequest;
import com.example.SHOP.dto.response.CustomerResponse;
import com.example.SHOP.models.CustomerModel;

@Service
public interface ICustomerService {

    Page<CustomerModel> findAllCustomers(Pageable pageable);

    Page<CustomerModel> findCustomersByName(String customerName, Pageable pageable);

    List<CustomerResponse> convertToCustomerResponse(Page<CustomerModel> customerModels);

    CustomerResponse findCustomerByCustomerId(CustomerRequest customerId);

    CustomerModel addCustomer(AddCustomerRequest addCustomerRequest);

    CustomerModel updateCustomer(Long customerId, UpdateCustomerRequest updateCustomerRequest);

    CustomerModel deleteCustomer(Long customerId);
    
}
