package com.example.SHOP.controllers;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import com.example.SHOP.dto.pagination.ItemPagination;
import com.example.SHOP.dto.pagination.ItemsInfo;
import com.example.SHOP.dto.request.AddItemRequest;
import com.example.SHOP.dto.request.ItemRequest;
import com.example.SHOP.dto.request.UpdateItemRequest;
import com.example.SHOP.dto.response.ItemListResponse;
import com.example.SHOP.dto.response.ItemResponse;
import com.example.SHOP.models.ItemModel;
import com.example.SHOP.services.IItemService;
import com.example.SHOP.services.specifications.ItemSortOrder;

@RestController
@RequestMapping("/")
public class ItemController {
    @Autowired
    private IItemService iItemService;

    @GetMapping("items")
    public ResponseEntity<?> getAllItems(@RequestParam(required = false) String itemName,
            @RequestParam(required = false) ItemSortOrder sortOrder,
            @RequestParam(defaultValue = "1") Integer pageNo,
            @RequestParam(defaultValue = "10") Integer pageSize) {
        Sort sort;
        if (sortOrder != null) {
            sort = Sort.by(Sort.Order.by(sortOrder.getFieldName()).with(sortOrder.getDirection()));
        } else {
            sort = Sort.by(Sort.Order.by("itemName").with(Sort.Direction.ASC));
        }
        Pageable paging = PageRequest.of(pageNo - 1, pageSize, sort);
        Page<ItemModel> pageResult;

        if (itemName != null && !itemName.isEmpty()) {
            pageResult = iItemService.findItemsByName(itemName, paging);
        } else {
            pageResult = iItemService.findAllItems(paging);
        }

        List<ItemResponse> itemResponse = iItemService.convertToItemResponse(pageResult);
        ItemsInfo itemsInfo = new ItemsInfo(
                itemResponse != null ? itemResponse.size() : 0,
                (int) pageResult.getTotalElements(),
                pageSize);

        int lastVisiblePage = pageResult.getTotalPages() > 0 ? pageResult.getTotalPages() : 1;

        ItemPagination paginationInfo = new ItemPagination(
                pageNo,
                pageResult.hasNext(),
                itemsInfo,
                lastVisiblePage);

        ItemListResponse response = new ItemListResponse(itemResponse, paginationInfo);

        if (itemResponse == null || itemResponse.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("item/{itemId}")
    public ResponseEntity<Object> getItemById(@PathVariable Long itemId) {
        ItemRequest itemRequest = new ItemRequest(itemId);
        ItemResponse item = iItemService.findByItemByItemId(itemRequest);

        Map<String, Object> responseBody = new HashMap<>();
        responseBody.put("item", item);
        responseBody.put("message", "Item with ID " + itemId + " Found");
        return ResponseEntity.ok(responseBody);
    }

    @PostMapping("item")
    public ResponseEntity<ItemModel> addItem(@RequestBody AddItemRequest addItemRequest,

            UriComponentsBuilder uriBuilder) {
        ItemModel itemModel = iItemService.addItem(addItemRequest);

        UriComponents uriComponents = uriBuilder.path("/item/{itemId}")
                .buildAndExpand(itemModel.getItemId());
        return ResponseEntity.created(uriComponents.toUri()).body(itemModel);
    }

    @PutMapping("item/{itemId}")
    public ResponseEntity<ItemModel> updateItem(@PathVariable Long itemId,
            @RequestBody UpdateItemRequest updateItemRequest) {
        ItemModel updatedItem = iItemService.updateItem(itemId, updateItemRequest);
        return ResponseEntity.ok(updatedItem);
    }

    @DeleteMapping("item/{itemId}")
    public ResponseEntity<Map<String, Object>> deleteItem(@PathVariable Long itemId) {
        iItemService.deleteItem(itemId);
        Map<String, Object> responseBody = new HashMap<>();
        responseBody.put("path", "/item/" + itemId);
        responseBody.put("error", "Success");
        responseBody.put("message", "Item with ID " + itemId + " has been successfully deleted.");
        responseBody.put("timestamp", LocalDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME));
        responseBody.put("status", HttpStatus.OK.value());

        return ResponseEntity.ok(responseBody);
    }
}
