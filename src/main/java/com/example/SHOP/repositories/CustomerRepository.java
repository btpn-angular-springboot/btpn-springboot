package com.example.SHOP.repositories;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.SHOP.models.CustomerModel;

@Repository
public interface CustomerRepository extends JpaRepository<CustomerModel, Long> {

    Page<CustomerModel> findAllByIsActiveTrue(Pageable pageable);

    Page<CustomerModel> findByCustomerNameContainingIgnoreCase(String customerName, Pageable pageable);

    Optional<CustomerModel> findByCustomerId(Long customerId);

}
